var s = document.body || document.documentElement, s = s.style, prefixAnimation = '', prefixTransition = '';

if( s.WebkitAnimation == '' )	prefixAnimation	 = '-webkit-';
if( s.MozAnimation == '' )		prefixAnimation	 = '-moz-';
if( s.OAnimation == '' )		prefixAnimation	 = '-o-';

if( s.WebkitTransition == '' )	prefixTransition = '-webkit-';
if( s.MozTransition == '' )		prefixTransition = '-moz-';
if( s.OTransition == '' )		prefixTransition = '-o-';

Object.prototype.onCSSAnimationEnd = function( callback )
{
	var runOnce = function( e ){ callback(); e.target.removeEventListener( e.type, runOnce ); };
	this.addEventListener( 'webkitAnimationEnd', runOnce );
	this.addEventListener( 'mozAnimationEnd', runOnce );
	this.addEventListener( 'oAnimationEnd', runOnce );
	this.addEventListener( 'oanimationend', runOnce );
	this.addEventListener( 'animationend', runOnce );
	if( ( prefixAnimation == '' && !( 'animation' in s ) ) || getComputedStyle( this )[ prefixAnimation + 'animation-duration' ] == '0s' ) callback();
	return this;
};

function startLogoAnimation(logo) {
	if (logo.classList.contains("animation-pause")) {
		logo.classList.remove("animation-pause");
	}
	logo.classList.add("animation-running");
}

function stopLogoAnimation(logo) {
	if (logo.classList.contains("animation-running")) {
		logo.classList.remove("animation-running");
	}
	logo.classList.add("animation-pause");
}

function toggleEndLogoAnimation(logo) {
	if (logo.classList.contains("animation-full")) {
		logo.classList.remove("animation-full");
	}
	logo.classList.toggle("animation-end");
}

function removeLogoSizeClass(logo) {
	var sizes = ["device-desktop", "device-tablet", "device-phone"];
	for (size of sizes) {
		if (logo.classList.contains(size)) {
			logo.classList.remove(size);
		}
	}
}

function isLogoAnimationFinished(logo) {
}

function resize(logo, width) {
	removeLogoSizeClass(logo);
	if(width > 1024) {
		logo.classList.add("device-desktop");
	} else if (width > 768) {
		logo.classList.add("device-tablet");
	} else {
		logo.classList.add("device-phone");
	}
}

var LOGO_ANIMATION_ENDED = false;

function fisherYates( array ){
	var count = array.length,
		randomnumber,
		temp;
	while( count ){
		randomnumber = Math.random() * count-- | 0;
		temp = array[count];
		array[count] = array[randomnumber];
		array[randomnumber] = temp
	}
	return array;
}

function init() {
	var logo = document.getElementById("logo");
	var width = window.outerWidth;

	resize(logo, width);
	

	logo.addEventListener("mouseover", function (event) {
		startLogoAnimation(logo);
	});

	logo.onCSSAnimationEnd(function() {
		LOGO_ANIMATION_ENDED = true;
		toggleEndLogoAnimation(logo);
	});

	var order = fisherYates([0, 1, 2, 3]);
	console.log(order);

	for (i = 0; i < 4; i++) {
		var icon_id = "icon" + i;
		icon = document.getElementById(icon_id);
		icon.style.animationDuration = 1150 + "ms";
		icon.style.animationDelay = 2000 + order[i] * 250 + "ms";
		icon.style.animationFillMode = "forwards";
	}
}

var onresize = function (e) {
	var logo = document.getElementById("logo");
	var width = e.target.outerWidth;

	if(LOGO_ANIMATION_ENDED){
		toggleEndLogoAnimation(logo);
	}
	resize(logo, width);
}

window.addEventListener('resize', onresize);


window.onload = function() {
	init();
};
